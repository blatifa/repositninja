package tpninja;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.util.List;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TpNinjaTest {
	WebDriver driver;
	String mail1 = "MyMail1@yahoo.fr";
	String mail2 = "MyMail2@yahoo.fr";

//Exigence No 01: L'utilisateur peut s'inscrire dans l'application en fournissant tous les champs.
	@Test
	public void register() throws InterruptedException {
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Register']")).click();
		driver.findElement(By.id("input-firstname")).sendKeys("yamin");
		driver.findElement(By.id("input-lastname")).sendKeys("mezghache");
		driver.findElement(By.id("input-email")).sendKeys(mail1);
		driver.findElement(By.id("input-telephone")).sendKeys("438-000-1111");
		driver.findElement(By.id("input-password")).sendKeys("y123456");
		driver.findElement(By.id("input-confirm")).sendKeys("y123456");
		Actions action1 = new Actions(driver);
		WebElement chekBox = driver.findElement(By.xpath("//input[@name='agree']"));
		action1.click(chekBox).build().perform();
		action1.sendKeys(Keys.TAB).build().perform();
		action1.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(4000);
		
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Continue']")).click();
		

	}

//Exigence no 02 : l'utilisateur peut s'inscrire à l'application en optant pour l'abonnement à la Newsletter
	@Test
	public void registerNewsletter() throws InterruptedException {
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Register']")).click();
		driver.findElement(By.id("input-firstname")).sendKeys("yamin");
		driver.findElement(By.id("input-lastname")).sendKeys("mezghache");
		driver.findElement(By.id("input-email")).sendKeys(mail2);
		driver.findElement(By.id("input-telephone")).sendKeys("438-000-1111");
		driver.findElement(By.id("input-password")).sendKeys("y123456");
		driver.findElement(By.id("input-confirm")).sendKeys("y123456");
		Actions action1 = new Actions(driver);
		WebElement radio = driver.findElement(By.xpath("//input[@name='newsletter']"));
		action1.click(radio).build().perform();
		WebElement chekBox = driver.findElement(By.xpath("//input[@name='agree']"));
		action1.click(chekBox).build().perform();
		action1.sendKeys(Keys.TAB).build().perform();
		action1.sendKeys(Keys.ENTER).build().perform();
	
		Thread.sleep(4000);

	}

//Exigence no 03 : L'utilisateur ne peut pas enregistrer un compte en double.
	@Test
	public void registerDoubleCompte() throws InterruptedException {
		String msg1 = " Warning: E-Mail Address is already registered!";

		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Register']")).click();
		driver.findElement(By.id("input-firstname")).sendKeys("yamin");
		driver.findElement(By.id("input-lastname")).sendKeys(mail2);
		driver.findElement(By.id("input-email")).sendKeys("mezghacheyamin@gmail.com");
		driver.findElement(By.id("input-telephone")).sendKeys("438-000-1111");
		driver.findElement(By.id("input-password")).sendKeys("y123456");
		driver.findElement(By.id("input-confirm")).sendKeys("y123456");
		Actions action1 = new Actions(driver);
		WebElement chekBox = driver.findElement(By.xpath("//input[@name='agree']"));
		action1.click(chekBox).build().perform();
		action1.sendKeys(Keys.TAB).build().perform();
		action1.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(4000);

		SoftAssert sa1 = new SoftAssert();
		sa1.assertEquals(driver.findElement(By.xpath("//div[text()=' Warning: E-Mail Address is already registered!']"))
				.getText().compareTo(msg1), true);

	}

//Exigence no 04 : l'utilisateur peut se connecter avec des informations d'identification valides
	@Test
	public void login() throws InterruptedException {
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Login']")).click();
		driver.findElement(By.id("input-email")).sendKeys("mezghacheyamin@gmail.com");
		driver.findElement(By.id("input-password")).sendKeys("y123456");
		driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
		
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Continue']")).click();
		Thread.sleep(4000);
		
		
		
		
	}

// Exigence no 05 : l'utilisateur est en mesure de réinitialiser le mot de passe oublié
	@Test
	public void forgottenPassword() throws InterruptedException {
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Login']")).click();
		driver.findElement(By.xpath("//div[@class='form-group']/a[text()='Forgotten Password']")).click();
		driver.findElement(By.id("input-email")).sendKeys("mezghacheyamin@gmail.com");
		driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
		Thread.sleep(4000);
	}

//Exigence no 06 : l'utilisateur est en mesure de rechercher des produits

	@Test
	public void research() throws InterruptedException {
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//input[@placeholder='Search']")).sendKeys("phone");
		driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg']")).click();
		Thread.sleep(4000);
	}

//Exigence no 07 : l'Utilisateur est informé lorsque le produit recherché n'est pas disponible

	@Test
	public void nonDisponible() throws InterruptedException {
		String msg = "There is no product that matches the search criteria.";

		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//input[@placeholder='Search']")).sendKeys("sumsungwwwww");
		driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg']")).click();

		SoftAssert sa = new SoftAssert();
		sa.assertEquals(
				driver.findElement(By.xpath("//p[text()='There is no product that matches the search criteria.']"))
						.getText().compareTo(msg),
				true);

		Thread.sleep(4000);
	}

//Exigence no 08 : l'utilisateur peut passer une commande

	@Test
	public void PasserCommande() throws InterruptedException {

		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");

		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Login']")).click();
		driver.findElement(By.id("input-email")).sendKeys("latifa@yahoo.fr");
		driver.findElement(By.id("input-password")).sendKeys("123456789");
		driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Cameras']")).click();
		driver.findElement(By.xpath("//h4/a[text()='Nikon D300']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[text()='Add to Cart']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Shopping Cart']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Checkout']")).click();
		Thread.sleep(2000);
		// step2
		driver.findElement(By.xpath("//input[@name='payment_address'][@value='new']")).click();

		driver.findElement(By.xpath("//input[@id='input-payment-firstname']")).sendKeys("latifa");
		driver.findElement(By.xpath("//input[@id='input-payment-lastname']")).sendKeys("boukehili");
		driver.findElement(By.xpath("//input[@id='input-payment-address-1']")).sendKeys("mon adresse");
		driver.findElement(By.xpath("//input[@id='input-payment-city']")).sendKeys("Montreal");
		driver.findElement(By.xpath("//input[@id='input-payment-postcode']")).sendKeys("A1A 2B2");

		List<WebElement> listeContries = driver.findElements(By.xpath("//select[@id='input-payment-country']/option"));
		for (WebElement Contry : listeContries) {
			if (Contry.getText().trim().equals("Canada")) {
				Contry.click();
				break;
			}
		}
		Thread.sleep(2000);
		List<WebElement> listeRegions = (List<WebElement>) driver
				.findElements(By.xpath("//select[@id='input-payment-zone']/option"));
		for (WebElement region : listeRegions) {
			if (region.getText().trim().equals("Québec")) {
				region.click();
				break;
			}
		}

		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='button-payment-address']")).click();
		Thread.sleep(2000);

		// step3
		driver.findElement(By.xpath("//input[@name='shipping_address'][@checked='checked']")).click();
		driver.findElement(By.xpath("//input[@id='button-shipping-address']")).click();

		// step4
		driver.findElement(By.xpath("//input[@id='button-shipping-method']")).click();
		// step5
		driver.findElement(By.xpath("//input[@name='agree']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='button-payment-method']")).click();
		Thread.sleep(2000);
		// step6
		driver.findElement(By.xpath("//input[@id='button-confirm']")).click();

		driver.findElement(By.xpath("//a[text()='Continue']")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Continue']")).click();
		Thread.sleep(2000);
	}

	///////////////////////////////////////////////////////////////////////////////////////

	@BeforeClass
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
